<?php

namespace App\Http\Requests;

use App\Traits\ErrorResponseTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class ParentRequest extends FormRequest
{
    use ErrorResponseTrait;

    protected function send_errors($validator_errors)
    {
        $errors = (new ValidationException($validator_errors))->errors();

        return $this->sendError($errors, __('messages.validation_error'));
    }
}
