<?php

namespace App\Http\Requests\Product;

use App\Http\Requests\ParentRequest;

class ProductRequest extends ParentRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'nullable|exists:categories,id',
            'name' => 'required|string|min:3|max:255',
            'price' => 'required|numeric',
            'discount' => 'nullable|numeric',
            'description' => 'nullable',
            'image_url' => 'nullable|mimes:jpg,jpeg,svg,bmp,png',
            'country' => 'nullable|string|min:3|max:255',
            'status' => 'nullable|boolean',
            'date_of_manufacture' => 'required|date',
            'validity_period' => 'required|date',
        ];
    }
}
