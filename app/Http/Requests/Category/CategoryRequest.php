<?php

namespace App\Http\Requests\Category;

use App\Http\Requests\ParentRequest;
use Illuminate\Contracts\Validation\Validator;

class CategoryRequest extends ParentRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3|max:255',
            'status' => 'nullable|integer',
        ];
    }

    protected function failedValidation(Validator $validator_errors)
    {
        return $this->send_errors($validator_errors);
    }
}
