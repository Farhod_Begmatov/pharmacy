<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Category\CategoryRequest;
use App\Http\Resources\Category\CategoryIndexResource;
use App\Http\Controllers\Controller;
use App\Models\Category\Category;

class CategoryController extends Controller
{

    protected $category;

    protected $per_page;

    public function __construct(Category $category)
    {
        $this->category = $category;

        $this->per_page = $this->per_page();
    }

    public function index()
    {
        $categories = $this->category;

        $categories = request('search') ? $categories->search(request('search')) : $categories;

        $categories = $categories->filter();

        $categories = $categories->sort()->paginate($this->per_page);

        return $this->respondSuccess([
            'categories' => CategoryIndexResource::collection($categories),
            'pagination' => $this->pagination($categories)
        ]);
    }

    public function store(CategoryRequest $request)
    {
        $request = $request->validated();

        $category = $this->category::create($request);

        return $this->respondStored([
            'category' => new CategoryIndexResource($category),
        ]);
    }

    public function show($id)
    {
        if (!$category = $this->category::where('id', $id)->first())
        {
            return $this->respondNotFound();
        }

        return $this->respondSuccess([
            'category' => new CategoryIndexResource($category),
        ]);
    }

    public function update(CategoryRequest $request, $id)
    {
        if (!$category = $this->category::where('id', $id)->first())
        {
            return $this->respondNotFound();
        }

        $request = $request->validated();

        $category->update($request);

        return $this->respondUpdated([
            'category' => new CategoryIndexResource($category)
        ]);
    }

    public function destroy($id)
    {
        if (!$category = $this->category->find($id))
        {
            return $this->respondNotFound();
        }

        $category->delete();

        return $this->respondDeleted();
    }

}
