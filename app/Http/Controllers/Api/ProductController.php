<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Product\ProductRequest;
use App\Http\Resources\Product\ProductIndexResource;
use App\Models\Product\Product;

class ProductController extends Controller
{
    protected $product;

    protected $per_page;

    public function __construct(Product $product)
    {
        $this->product = $product;

        $this->per_page = $this->per_page();
    }
    public function index(): object
    {
        $products = $this->product;

        $products = request('search') ? $products->search(request('search')) : $products;

        $products = $products->filter();

        $products = $products->sort()->paginate($this->per_page);

        return $this->respondSuccess([
            'products' => ProductIndexResource::collection($products),
            'pagination' => $this->pagination($products)
        ]);
    }

    public function store(ProductRequest $request): object
    {
        $request = $request->validated();

        if(isset($request['discount'])){
            $request['price_with_discount'] = $request['price'] - $request['discount'];
        }

        $product = $this->product::create($request);

        return $this->respondStored([
            'product' => new ProductIndexResource($product),
        ]);
    }

    public function show($id): object
    {
        if (!$product = $this->product::where('id', $id)->first())
        {
            return $this->respondNotFound();
        }

        return $this->respondSuccess([
            'product' => new ProductIndexResource($product),
        ]);
    }

    public function update(ProductRequest $request, $id): object
    {
        if (!$product = $this->product::where('id', $id)->first())
        {
            return $this->respondNotFound();
        }

        $request = $request->validated();

        if(isset($request['discount'])){
            $request['price_with_discount'] = $request['price'] - $request['discount'];
        }

        $product->update($request);

        return $this->respondUpdated([
            'product' => new ProductIndexResource($product)
        ]);
    }

    public function destroy($id): object
    {
        if (!$product = $this->product->find($id))
        {
            return $this->respondNotFound();
        }

        $product->delete();

        return $this->respondDeleted();
    }
}
