<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponseTrait;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Cache;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use ApiResponseTrait, AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function per_page()
    {
        return request()->get('per_page', 1000000);
    }

    public function pagination(object $object): array
    {
        return [
            'total' => $object->total(),
            'count' => $object->count(),
            'per_page' => $object->perPage(),
            'current_page' => $object->currentPage(),
            'total_pages' => $object->lastPage(),
            'last_page' => $object->lastPage()
        ];
    }

    public function cache()
    {
        return Cache::class;
    }
}
