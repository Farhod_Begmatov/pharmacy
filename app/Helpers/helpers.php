<?php

/**
 *
 *   GET FUNCTIONS
 */


use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

if (!function_exists('getDotDateFormat')) {
    function getDotDateFormat(): string
    {
        return 'd.m.Y H:i:s';
    }
}
if (!function_exists('getDotDate')) {
    /**
     * @return string d.m.Y H:i:s format
     */
    function getDotDate()
    {
        return Carbon::now()->format(getDotDateFormat());
    }
}
if (!function_exists('toDateString')) {
    function toDateString($date)
    {
        return Carbon::parse($date)->toDateString();
    }
}
//File Validation Helpers
if (!function_exists('getDocumentValidation')) {
    function getDocumentValidation(): string
    {
        return "mimes:doc,docx,pdf,jpg,jpeg,gif,svg,png|max:2048";
    }
}
if (!function_exists('getTodayDate')) {
    function getTodayDate()
    {
        return Carbon::now()->format('d.m.Y');
    }
}

if (!function_exists('getDayAgo')) {
    function getDayAgo(int $day = 1)
    {
        return Carbon::now()->subDays($day)->format('d.m.Y');
    }
}
if (!function_exists('getDayAgoInWord')) {
    function getDayAgoInWord(int $day = 1)
    {
        return Str::ucfirst(Carbon::now()->subDays($day)->translatedFormat('l'));
    }
}

if (!function_exists('getMonthAgoByDays')) {
    function getMonthAgoByDays(int $month = 1)
    {
        return Carbon::now()->subDays($month)->format('d.m.Y');
    }
}
if (!function_exists('getMonthAgoByMonthsInWords')) {
    function getMonthAgoByMonthsInWords(int $month = 1)
    {
        return Str::ucfirst(Carbon::now()->subMonths($month)->translatedFormat('F'));
    }
}

if (!function_exists('getYearAgo')) {

    function getYearAgo(int $year = 1)
    {
        return Carbon::now()->subYears($year)->format('Y-m-d');
    }
}

if (!function_exists('getYearAgoFormat')) {

    function getYearAgoFormat(int $year = 1,string $format)
    {
        return Carbon::now()->subMonths($year)->format($format);
    }
}

if (!function_exists('getImageValidation')) {
    function getImageValidation(): string
    {
        return "mimes:jpg,jpeg,gif,svg,png|max:2048";
    }
}

if (!function_exists('getImageAndDocumentValidation')) {
    function getImageAndDocumentValidation(): string
    {
        return "mimes:doc,docx,pdf,jpg,jpeg,gif,svg,png|max:2048";
    }
}
//End of File Validation Helpers

//Get current Currency
if (!function_exists('getCurrentCurrency')) {
    function getCurrentCurrency()
    {
        return  \App\Models\Settings\Currency\Currency::where('active', true)->first();
    }
}

//get authenticated user
if (!function_exists('getCurrentUser')) {
    function getCurrentUser()
    {
        return Auth::user();
    }
}
/**
 *
 * List of Controllers that are used Api Development
 */
if (!function_exists('getControllerList')) {
    /**
     *
     * @return array
     */
    function getControllerList(): array
    {
        $controllers = [];

        foreach (Route::getRoutes()->getRoutes() as $route) {

            $action = $route->getAction();

            logger($action);

            if (array_key_exists('controller', $action) && preg_match('/api/i', $action['prefix'])) {

                if (!in_array($action['controller'], App\Models\User\Permission\Permission::permission_exceptions)) {

                    $controllers[] = substr(strrchr($action['controller'], "\\"), 1);
                }
            }
        }

        return $controllers;
    }
}

/**
 *
 *
 * END OF GET FUNCTIONS
 */


/**
 *
 * Make Functions
 */
if (!function_exists('makeSlug')) {
    /**
     * @param string
     * @return string
     */
    function makeSlug($slug)
    {
        return Str::slug($slug, '-', app()->getLocale());
    }
}
if (!function_exists('convertDotDate')) {
    /**
     * @param date
     * @return string d.m.Y H:i:s format
     */
    function convertDotDate($date): string
    {
        return Carbon::parse($date)->format(getDotDateFormat());
    }
}

/**
 *
 * Custom Array Functions
 */
if (!function_exists('array_remove')) {
    /**
     * Remove unnecessary elements from array
     * @param array removable elements
     * @param array array that needs to be removed from
     * @return array array
     */

    function array_remove(array $delete_array, array $array): array
    {
        foreach ($delete_array as $deleteItem) {
            unset($array[$deleteItem]);
        }

        return $array;
    }
}
/**
 *
 * End of Custom Array Functions
 */
if (!function_exists('makeNotFoundMessage')) {
    function makeNotFoundMessage($name): string
    {
        return __('messages.not_found', ['name' => $name]);
    }
}

if (!function_exists('makeCreatedMessage')) {
    function makeStoredMessage($name): string
    {
        return __('messages.store_success', ['name' => $name]);
    }
}

if (!function_exists('makeUpdateSuccessMessage')) {
    function makeUpdatedMessage($name): string
    {
        return __('messages.update_success', ['name' => $name]);
    }
}

if (!function_exists('makeDeletedMessage')) {
    function makeDeletedMessage($name)
    {
        return __('messages.destroy_success', ['name' => $name]);
    }
}
if (!function_exists('makeBadRequestMessage')) {
    function makeBadRequestMessage($name)
    {
        return __('messages.bad_request', ['name' => $name]);
    }
}

if (!function_exists('makeHashed')) {
    function makeHashed($value): string
    {
        return Hash::make($value);
    }
}

if (!function_exists('canUserBeDeleted')) {
    function canUserBeDeleted(object $user): bool
    {
        if (!empty($user->userRole) && !empty($user->userRole->role->whereIn('name', ['Super Admin', 'Admin', 'Project Manager']))) {

            return true;
        }
        return false;
    }
}


if (!function_exists('hasTable')) {
    function hasTable(string $table): bool
    {
        return Schema::hasTable($table);
    }
}

if (!function_exists('hasTables')) {
    function hasTables(array $tables): bool
    {
        foreach ($tables as $table) {
            if (!(Schema::hasTable($table))) {
                return false;
            }
        }

        return true;
    }
}

