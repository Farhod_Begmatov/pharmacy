<?php

namespace App\Models;

use App\Traits\ScopeTrait;
use App\Traits\Category\CategoryFilter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ParentModel extends Model
{
    use ScopeTrait, SoftDeletes, HasFactory, CategoryFilter;

    protected $guarded = [];

    public function getCreatedAtAttribute()
    {
        return convertDotDate($this->attributes['created_at']);
    }

    public function getUpdatedAtAttribute()
    {
        return convertDotDate($this->attributes['updated_at']);
    }

    public function getDeletedAtAttribute()
    {
        return convertDotDate($this->attributes['deleted_at']);
    }

    public function getTableName()
    {
        return $this->table;
    }
}
