<?php

namespace App\Models\Category;

use App\Models\Product\Product;
use App\Models\ParentModel;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;

/**
 * @method static where(string $string, $id)
 * @method static create(array $request)
 * @method search(array|Application|Request|string|null $request)
 * @method filter()
 * @method find($id)
 */
class Category extends ParentModel
{
    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
