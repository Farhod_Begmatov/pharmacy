<?php

namespace App\Models\Product;

use App\Models\ParentModel;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;

/**
 * @method search(array|Application|Request|string|null $request)
 * @method filter()
 * @method static create(array $request)
 * @method static where(string $string, $id)
 * @method find($id)
 */
class Product extends ParentModel
{

}
