<?php


namespace App\Traits;

trait ScopeTrait
{
    /**
     * @param $query
     * @return mixed
     */
    public function scopeSort($query){
        return $query->orderBy(request()->get('column', 'id'), request()->get('order', 'asc'));
    }

    /**
     * @param $query
     * @param $string
     * @return mixed
     */
    public function scopeSearch($query, $string){

        $columns = $this->search_columns;

        return $query->where(function ($query) use($string, $columns) {
            foreach ($columns as $column){
                $query->orwhere($column, 'ilike',  '%' . $string .'%');
            }
        });
    }


}

