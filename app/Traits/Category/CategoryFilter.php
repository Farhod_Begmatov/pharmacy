<?php

namespace App\Traits\Category;

trait CategoryFilter
{
    public $search_columns = [
        'id',
        'name'
    ];

    public function scopeFilter($query)
    {

        if ($filter = request('id')) {
            $query = $query->where('id', 'ilike', '%' .  $filter . '%');
        }
        if ($name = request('name')) {
            $query = $query->where('name', 'ilike', "%$name%");
        }
        if ($from = request()->get('created_at', null)) {
            $query = $query->whereDate('created_at', '=', toDateString($from));
        }
        if ($filter = request('updated_at')) {
            $query = $query->whereDate('updated_at', '=', toDateString($filter));
        }
        if ($from = request()->get('from_date', null)) {
            $query = $query->whereDate('created_at', '>=', toDateString($from));
        }
        if ($to = request()->get('to_date', null)) {
            $query = $query->whereDate('created_at', '<=', toDateString($to));
        }

        return $query;
    }
}
