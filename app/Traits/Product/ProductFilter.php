<?php

namespace App\Traits\Product;

trait ProductFilter
{
    public $search_columns = [
        'id',
        'name',
        'description',
    ];

    public function scopeFilter($query)
    {

        if ($filter = request('id')) {
            $query = $query->where('id', 'ilike', '%' .  $filter . '%');
        }
        if ($name = request('name')) {
            $query = $query->where('name', 'ilike', "%$name%");
        }
        if ($country = request('country')) {
            $query = $query->where('country', 'ilike', "%$country%");
        }
        if ($description = request('description')) {
            $query = $query->where('description', 'ilike', "%$description%");
        }
        if ($price = request('price')) {
            $query = $query->where('price', 'ilike', "%$price%");
        }
        if ($price_with_discount = request('price_with_discount')) {
            $query = $query->where('price_with_discount', 'ilike', "%$price_with_discount%");
        }
        if ($discount = request('discount')) {
            $query = $query->where('discount', 'ilike', "%$discount%");
        }
        if ($date = request()->get('date_of_manufacture', null)) {
            $query = $query->whereDate('date_of_manufacture', '=', toDateString($date));
        }
        if ($date = request('validity_period')) {
            $query = $query->whereDate('validity_period', '=', toDateString($date));
        }
        if ($from = request()->get('created_at', null)) {
            $query = $query->whereDate('created_at', '=', toDateString($from));
        }
        if ($filter = request('updated_at')) {
            $query = $query->whereDate('updated_at', '=', toDateString($filter));
        }
        if ($from = request()->get('from_date', null)) {
            $query = $query->whereDate('created_at', '>=', toDateString($from));
        }
        if ($to = request()->get('to_date', null)) {
            $query = $query->whereDate('created_at', '<=', toDateString($to));
        }

        return $query;
    }
}
