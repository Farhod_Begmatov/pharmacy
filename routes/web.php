<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.index');
});

Route::get('/about', function () {
    return view('pages.about');
});

Route::get('/contact', function () {
    return view('pages.contact');
});

Route::get('/categories', function () {
    return view('pages.categories');
});

Route::get('/categories/{category}', function () {
    return view('pages.category');
});

Route::get('/medications', function () {
    return view('pages.medications');
});

Route::get('/medications/{id}', function () {
    return view('pages.medication');
});

//Route::get('/medication', function () {
//    return view('pages.medication');
//});

Route::get('/card', function () {
    return view('pages.card');
});

Route::get('/checkout', function () {
    return view('pages.checkout');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
