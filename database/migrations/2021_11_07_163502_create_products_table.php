<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('category_id')->nullable()->constrained();
            $table->string('name');
            $table->double('price',15,2);
            $table->double('discount',15,2)->nullable();
            $table->double('price_with_discount',15,2)->nullable();
            $table->text('description')->nullable();
            $table->string('image_url')->nullable();
            $table->string('country')->nullable();
            $table->boolean('status')->default(true);
            $table->date('date_of_manufacture');
            $table->date('validity_period');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
